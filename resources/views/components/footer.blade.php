<div class="row justify-content-center pt-2">
	<div class="col text-center">
		&copy; {{ date('Y') }} &nbsp;
		<a href="{{ url('https://www.genesedan.com/') }}" 
			class="text-secondary" 
			target="__blank">
			{{ __('Gene Sedan') }}
		</a>
	</div>
</div>